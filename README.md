# Jenkins Helm Chart

Fork adapted from:
https://github.com/kubernetes/charts/tree/master/stable/jenkins

Jenkins master and slave cluster utilizing the Jenkins Kubernetes plugin

* https://wiki.jenkins-ci.org/display/JENKINS/Kubernetes+Plugin

Inspired by the awesome work of Carlos Sanchez <carlos@apache.org>

## Chart Details
This chart will do the following:

* 1 x Jenkins Master with port 8080 exposed on an external LoadBalancer
* All using Kubernetes Deployments

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install --name my-release stable/jenkins
```

**Fork notes:**

or set `PUBLIC_IP` in `my_deploy_script` and use this script to expose the
services in a less sophisticated but simpler, cloud-agnostic way

## Configuration

The following tables lists the configurable parameters of the Jenkins chart and their (modified from stable upstream) default values.

**Fork notes:**

Changes from upstream default values are denoted in **`bold`**

### Jenkins Master

| Parameter                  | Description                        | Default                                                    |
| -----------------------    | ---------------------------------- | ---------------------------------------------------------- |
| `Master.Name`              | Jenkins master name                | `jenkins-master`                                           |
| `Master.Image`             | Master image name                  | **`yebyen/jenkins-master`**                                |
| `Master.ImageTag`          | Master image tag                   | **`v0.5.0`**                                               |
| `Master.ImagePullPolicy`   | Master image pull policy           | `Always`                                                   |
| `Master.Component`         | k8s selector key                   | `jenkins-master`                                           |
| `Master.Cpu`               | Master requested cpu               | `200m`                                                     |
| `Master.Memory`            | Master requested memory            | `256Mi`                                                    |
| `Master.ServicePort`       | k8s service port                   | `8080`                                                     |
| `Master.ContainerPort`     | Master listening port              | `8080`                                                     |
| `Master.SlaveListenerPort` | Listening port for agents          | `50000`                                                    |

### Jenkins Agent

| Parameter               | Description                        | Default                                                    |
| ----------------------- | ---------------------------------- | ---------------------------------------------------------- |
| `Agent.Image`           | Agent image name                   | **`yebyen/jenkins-ruby-slave`**                            |
| `Agent.ImageTag`        | Agent image tag                    | **`v5`**                                                   |
| `Agent.Cpu`             | Agent requested cpu                | **`2`**                                                    |
| `Agent.Memory`          | Agent requested memory             | **`1Gi`**                                                  |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install --name my-release -f values.yaml stable/jenkins
```

> **Tip**: You can use the default [values.yaml](values.yaml)

**Fork notes:**

Find the Dockerfiles and sources/upstream links for the customized Jenkins images used here:

| Image                           | Git Repository                                             |
| ------------------------------- | ---------------------------------------------------------- |
| yebyen/jenkins-ruby-slave       | https://github.com/kingdonb/docker-jnlp-slave-ruby-runtime |
| yebyen/jenkins-ruby-slave-base  | https://github.com/kingdonb/jnlp-slave-ruby                |
| yebyen/docker-jnlp-slave        | https://github.com/kingdonb/docker-jnlp-slave              |
| yebyen/docker-slave             | https://github.com/kingdonb/docker-slave                   |

## Persistence

**Fork notes:**

The Jenkins image stores persistence under `/var/jenkins_home` path of the container. A Persistent Volume
Claim is used to keep the data **in `/data/jenkins`** across deployments. This is **for single-node
deployments** and will probably not work with multi-node deployments of any kind.  Find guidance on how
to delete a deployment and its associated PV in `my_cleanup_script`.

# Todo
* Enable Docker-in-Docker or Docker-on-Docker support on the Jenkins agents
